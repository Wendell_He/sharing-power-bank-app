﻿# 微信小程序
## 微信小程序电商源码：共享充电宝小程序。

#### 项目介绍

	此项目是一套可用于线上共享充电宝的租借系统

#### 功能介绍

	1. 可在前端小程序上自助租借充电宝
	2. 可在前端小程序上自助借还充电宝
	

    - Tip 更换页面，在app.json里面设置路径即可
	
### QQ交流群 — 24934459
### 公司官网 - http://www.fz33.net  官网
### 公司其他项目案例

***共享充电宝：https://gitee.com/sansanC/sharing-power-bank-app***

多门店派单：https://gitee.com/sansanC/multiple-stores-send-single-applet 

在线课程：https://gitee.com/sansanC/online-course-applet 

健身馆：https://gitee.com/sansanC/gym-app 

派单：https://gitee.com/sansanC/dispatch-applet 

场馆预定：https://gitee.com/sansanC/venue-booking-procedures 

社区团购小程序：https://gitee.com/sansanC/community-group-buying-app 

早餐线上预订：https://gitee.com/sansanC/breakfast-subscription-applet 

相册资源存储https://gitee.com/sansanC/photo-album-applet 

美容美发：https://gitee.com/sansanC/beauty-salon-small-program

商城小程序：https://gitee.com/sansanC/wechatApp

按摩小程序：https://gitee.com/sansanC/massage-applet

### 管理端小程序效果图（部分图）

|共享充电宝|共享充电宝
|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/14/49/37_221.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/14/49/37_205.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/14/49/37_61.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/14/49/36_638.jpg)

### 效果图---扫码查看

|扫码预览|扫码预览|扫码预览
|:----|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/34_901.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/33_675.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/33_601.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/33_426.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/33_828.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/33_761.jpg)


### 公司资质

|省高薪证书|国高薪证书|
|:----|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/8_826.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/6_933.jpg)
    

|![file-list](http://image1.sansancloud.com/xianhua/2021_4/2/15/11/33_110.jpg)